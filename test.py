from describe.core import System
from describe.descriptors import CoulombMatrix as CM, SineMatrix as SM, MTBR
from describe import utils
from ase.build import bulk
from fancydict import FancyDict as FD

atoms_a = [bulk('Si'), bulk('Cu')]
stats = FD(utils.atoms_stats(atoms_a))

nmax = 2
cmd = CM(n_atoms_max=nmax)
smd = SM(n_atoms_max=nmax)

cm_a = [cmd.create(atoms) for atoms in atoms_a]
sm_a = [cmd.create(atoms) for atoms in atoms_a]



